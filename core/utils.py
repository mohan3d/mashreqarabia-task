from datetime import date
from decimal import Decimal


def calculate_age(born_date):
    today = date.today()
    return today.year - born_date.year - ((today.month, today.day) < (born_date.month, born_date.day))


def calculate_default_deductions(position, salary):
    if position == 'E':
        return salary * Decimal(7.5 / 100)

    if position == 'M':
        return salary * Decimal(12 / 100)

    if position == 'C':
        return salary * Decimal(15 / 100)


def valid_salary(position, salary):
    if position == 'E':
        return 5000 <= salary <= 10000

    if position == 'M':
        return 10000 < salary <= 19000

    if position == 'C':
        return 19000 < salary <= 25000

    return False
