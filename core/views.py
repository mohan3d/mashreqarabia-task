from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import generic

from .forms import EmployeeForm, FamilyMemberForm, SalaryDetailForm, DeductionForm, EarningForm
from .models import Employee, FamilyMember, SalaryDetail


class EmployeeTerminateMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.get_employee().terminated:
            raise Http404('Terminated employee.')

        return super().dispatch(request, *args, **kwargs)

    def get_employee(self):
        return self.get_object()


class EmployeeSiblingMixin(EmployeeTerminateMixin):
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['employee'] = self.get_employee()
        return context

    def get_success_url(self):
        return reverse('core:sibling-list', kwargs={'employee_id': self.get_employee_id()})

    def get_employee(self):
        return get_object_or_404(Employee, pk=self.get_employee_id())

    def get_employee_id(self):
        return self.kwargs.get('employee_id')


class SalaryDetailMixin(EmployeeTerminateMixin):
    def get_initial(self):
        initial = super().get_initial()
        initial['salary_details'] = self.get_salary_detail()
        return initial

    def get_success_url(self):
        return reverse('core:salary-detail-view', kwargs={
            'pk': self.get_salary_detail_id()
        })

    def get_salary_detail(self):
        return get_object_or_404(SalaryDetail, pk=self.get_salary_detail_id())

    def get_salary_detail_id(self):
        return self.kwargs.get('salary_details_id')

    def get_employee(self):
        return self.get_salary_detail().employee


class EmployeeList(generic.ListView):
    model = Employee
    context_object_name = 'employees'
    template_name = 'core/employee_list.html'


class EmployeeCreate(generic.CreateView):
    form_class = EmployeeForm
    template_name = 'core/employee_create.html'
    success_url = reverse_lazy('core:employee-list')


class EmployeeUpdate(EmployeeTerminateMixin, generic.UpdateView):
    model = Employee
    form_class = EmployeeForm
    template_name = 'core/employee_update.html'
    success_url = reverse_lazy('core:employee-list')


class EmployeeDelete(EmployeeTerminateMixin, generic.DeleteView):
    model = Employee
    template_name = 'core/employee_delete.html'
    success_url = reverse_lazy('core:employee-list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()

        if not self.object.job:
            self.object.delete()
        else:
            self.object.terminated = True
            self.object.save()

        return HttpResponseRedirect(success_url)


class FamilyMemberList(EmployeeSiblingMixin, generic.ListView):
    template_name = 'core/sibling_list.html'
    context_object_name = 'members'

    def get_queryset(self):
        employee_id = self.kwargs.get('employee_id')
        employee = self.get_employee()

        if not employee.is_married:
            raise Http404('Not a married employee')

        return FamilyMember.objects.filter(employee_id=employee_id)


class FamilyMemberCreate(EmployeeSiblingMixin, generic.CreateView):
    form_class = FamilyMemberForm
    template_name = 'core/sibling_create.html'

    def get_initial(self):
        initial = super(FamilyMemberCreate, self).get_initial()
        initial['employee'] = self.get_employee()
        return initial


class FamilyMemberUpdate(EmployeeSiblingMixin, generic.UpdateView):
    model = FamilyMember
    form_class = FamilyMemberForm
    template_name = 'core/sibling_update.html'


class FamilyMemberDelete(EmployeeSiblingMixin, generic.DeleteView):
    model = FamilyMember
    template_name = 'core/sibling_delete.html'


class SalaryDetails(generic.DetailView):
    model = SalaryDetail
    context_object_name = 'salary_details'
    template_name = 'core/salary_details.html'


class SalaryDetailCreate(EmployeeSiblingMixin, generic.CreateView):
    form_class = SalaryDetailForm
    template_name = 'core/salary_detail_create.html'

    def get_initial(self):
        initial = super(SalaryDetailCreate, self).get_initial()
        initial['employee'] = self.get_employee()
        return initial

    def get_success_url(self):
        return reverse('core:salary-detail-view', kwargs={'pk': self.object.id})


class DeductionCreate(SalaryDetailMixin, generic.CreateView):
    form_class = DeductionForm
    template_name = 'core/deduction_create.html'


class EarningCreate(SalaryDetailMixin, generic.CreateView):
    form_class = EarningForm
    template_name = 'core/earning_create.html'
