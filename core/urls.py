from django.urls import include, path

from . import views

app_name = 'core'

urlpatterns = [
    path('employee/', include([
        path('list/', views.EmployeeList.as_view(), name='employee-list'),
        path('create/', views.EmployeeCreate.as_view(), name='employee-create'),
        path('<int:pk>/edit', views.EmployeeUpdate.as_view(), name='employee-update'),
        path('<int:pk>/delete', views.EmployeeDelete.as_view(), name='employee-delete'),
    ])),

    path('sibling/<int:employee_id>/', include([
        path('', views.FamilyMemberList.as_view(), name='sibling-list'),
        path('create/', views.FamilyMemberCreate.as_view(), name='sibling-create'),
        path('<int:pk>/update/', views.FamilyMemberUpdate.as_view(), name='sibling-update'),
        path('<int:pk>/delete/', views.FamilyMemberDelete.as_view(), name='sibling-delete'),
    ])),

    path('<int:pk>/salary-details/view', views.SalaryDetails.as_view(), name='salary-detail-view'),
    path('<int:employee_id>/salary-details/create', views.SalaryDetailCreate.as_view(), name='salary-detail-create'),

    path('<int:salary_details_id>/deduction/create', views.DeductionCreate.as_view(), name='deduction-create'),
    path('<int:salary_details_id>/earning/create', views.EarningCreate.as_view(), name='earning-create'),
]
