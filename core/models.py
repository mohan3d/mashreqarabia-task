from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models

from .utils import calculate_age, calculate_default_deductions


class Country(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Countries'


class Job(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Employee(models.Model):
    POSITION_CHOICES = (
        ('E', 'Employee'),
        ('M', 'Manager'),
        ('C', 'CEO')
    )

    MARITAL_STATUS_CHOICES = (
        ('S', 'Single'),
        ('M', 'Married')
    )

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )

    first_name = models.CharField(max_length=80)
    middle_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    job = models.ForeignKey(Job, null=True, blank=True, on_delete=models.SET_NULL)
    position = models.CharField(max_length=1, choices=POSITION_CHOICES)
    national_id = models.CharField(unique=True, max_length=settings.NATIONAL_ID_LENGTH,
                                   validators=[
                                       RegexValidator(
                                           regex=settings.NATIONAL_ID_REGEX,
                                           message='National id must be {} digits'.format(settings.NATIONAL_ID_LENGTH),
                                           code='invalid_national_id'
                                       )
                                   ])
    date_of_birth = models.DateField()
    place_of_birth = models.CharField(max_length=255)
    country = models.ForeignKey(Country, on_delete=models.PROTECT)
    nationality = models.CharField(max_length=100)
    marital_status = models.CharField(max_length=1, choices=MARITAL_STATUS_CHOICES)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    terminated = models.BooleanField(default=False, editable=False)

    @property
    def full_name(self):
        return '{} {} {}'.format(self.first_name,
                                 self.middle_name,
                                 self.last_name)

    @property
    def age(self):
        return calculate_age(self.date_of_birth)

    @property
    def is_married(self):
        return self.marital_status == "M"

    def __str__(self):
        return self.full_name


class FamilyMember(models.Model):
    RELATIONS_CHOICES = (
        ('C', 'Child'),
        ('W', 'Wife'),
        ('H', 'Husband')
    )

    relation_type = models.CharField(max_length=1, choices=RELATIONS_CHOICES)
    name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)

    @property
    def age(self):
        return calculate_age(self.date_of_birth)

    def __str__(self):
        return self.name


class SalaryDetail(models.Model):
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE)

    @property
    def main_salary(self):
        return self.amount

    @staticmethod
    def _aggregation_value(aggregation):
        value = aggregation.get('amount__sum')
        return value if value else 0

    @property
    def total_earnings(self):
        aggregation = self.earnings.aggregate(models.Sum('amount'))
        return self._aggregation_value(aggregation)

    @property
    def total_deductions(self):
        deductions = self.deductions

        if not deductions.exists():
            return calculate_default_deductions(self.employee.position, self.main_salary)

        aggregation = self.deductions.aggregate(models.Sum('amount'))
        return self._aggregation_value(aggregation)

    @property
    def total_salary(self):
        return self.main_salary + self.total_earnings - self.total_deductions

    def __str__(self):
        return 'Salary details for {}'.format(self.employee.full_name)


class Earning(models.Model):
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    reason = models.CharField(max_length=255, blank=True)
    salary_details = models.ForeignKey(SalaryDetail, on_delete=models.CASCADE, related_name='earnings')

    def __str__(self):
        return '{} for {}'.format(self.amount, self.reason)


class Deduction(models.Model):
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    reason = models.CharField(max_length=255, blank=True)
    salary_details = models.ForeignKey(SalaryDetail, on_delete=models.CASCADE, related_name='deductions')

    def __str__(self):
        return '- {} for {}'.format(self.amount, self.reason)
