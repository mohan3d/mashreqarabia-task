import datetime

from django import forms
from django.conf import settings

from .models import Employee, FamilyMember, SalaryDetail, Deduction, Earning
from .utils import valid_salary

VALID_YEARS = tuple(year for year in range(settings.MIN_BORN_YEAR, datetime.datetime.now().year + 1))


class EmployeeForm(forms.ModelForm):

    def clean_national_id(self):
        if self.instance.national_id:
            return self.instance.national_id
        return self.cleaned_data['national_id']

    class Meta:
        model = Employee
        fields = ['first_name', 'middle_name', 'last_name', 'job', 'position', 'national_id', 'date_of_birth',
                  'place_of_birth', 'country', 'nationality', 'marital_status', 'gender']

        widgets = {
            'date_of_birth': forms.SelectDateWidget(years=VALID_YEARS)
        }


class FamilyMemberForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.employee = kwargs['initial'].get('employee')
        super(FamilyMemberForm, self).__init__(*args, **kwargs)

    def clean_employee(self):
        if self.employee:
            return self.employee
        return self.cleaned_data['employee']

    class Meta:
        model = FamilyMember
        fields = ['relation_type', 'name', 'date_of_birth', 'employee']

        widgets = {
            'date_of_birth': forms.SelectDateWidget(years=VALID_YEARS),
            'employee': forms.TextInput(attrs={
                'readonly': 'true'
            })
        }


class SalaryDetailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.employee = kwargs['initial'].get('employee')
        super(SalaryDetailForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()

        position = self.employee.position
        amount = cleaned_data.get("amount")

        if position and amount:
            if not valid_salary(position, amount):
                raise forms.ValidationError('Invalid salary range.')

        return cleaned_data

    class Meta:
        model = SalaryDetail
        fields = ['amount', 'employee']

        widgets = {
            'employee': forms.TextInput(attrs={
                'readonly': 'true'
            })
        }


class DeductionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.salary_details = kwargs['initial'].get('salary_details')
        super(DeductionForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Deduction
        fields = ['amount', 'reason', 'salary_details']

        widgets = {
            'salary_details': forms.TextInput(attrs={
                'readonly': 'true'
            })
        }


class EarningForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.salary_details = kwargs['initial'].get('salary_details')
        super(EarningForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Earning
        fields = ['amount', 'reason', 'salary_details']

        widgets = {
            'salary_details': forms.TextInput(attrs={
                'readonly': 'true'
            })
        }
