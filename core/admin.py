from django.contrib import admin

from .models import Country, Job, Employee, FamilyMember, SalaryDetail, Deduction, Earning

admin.site.register(Country)
admin.site.register(Job)
admin.site.register(Employee)
admin.site.register(FamilyMember)
admin.site.register(SalaryDetail)
admin.site.register(Deduction)
admin.site.register(Earning)
