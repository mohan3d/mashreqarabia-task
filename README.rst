Mashreq Arabia Task
===================

python assignment.


Install
-------

.. code-block:: bash

    $ pip install -r requirements.txt


Usage
-----

Make sure you are running python3.5+ and installed requirements.

.. code-block:: bash

    $ python manage.py runserver


Note
----

Generate and set new SECRET_KEY in settings.py before running or deploying.

.. code-block:: bash

    $ python -c 'import random; print("".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)]))'